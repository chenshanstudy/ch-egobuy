package com.lh.buy.content.contfig;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author lh
 * @create 2021-04-16 18:04
 */
@Configuration
public class DruidConfig {
    @Bean
    @ConditionalOnMissingBean
    @ConfigurationProperties(prefix = "druid")
    public DataSource dataSource(){
        return new DruidDataSource();
    }
}

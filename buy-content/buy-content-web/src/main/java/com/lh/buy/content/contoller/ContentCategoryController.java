package com.lh.buy.content.contoller;

import com.lh.buy.content.service.ContentCategoryService;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.buy.util.pojo.EasyUITreeNode;
import com.lh.pojo.TbContentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 19:36
 */
@RestController
public class ContentCategoryController {
    @Autowired
    private ContentCategoryService contentCategoryService;

    @RequestMapping("/contentCategory/list")
    public List<EasyUITreeNode> queryByPid(long pid){
        return contentCategoryService.querybCatByPid(pid);
    }

    @RequestMapping("/contentCategory/delete")
    public String delete(long catId){
        contentCategoryService.delete(catId);
        return "ok";
    }

    @RequestMapping("/contentCategory/update")
    public String update(@RequestBody TbContentCategory contentCategory){
        contentCategoryService.update(contentCategory);
        return "ok";
    };

    @RequestMapping("/contentCategory/save")
    public BuyResult save(@RequestBody TbContentCategory contentCategory){
        contentCategoryService.save(contentCategory);
        BuyResult buyResult = BuyResult.ok();
        buyResult.setData(contentCategory);//将保存后的对象直接设置到BuyResult
        return buyResult;
    }
}

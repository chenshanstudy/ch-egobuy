package com.lh.buy.content.contoller;


import com.lh.buy.content.service.ContentService;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.pojo.TbContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 21:10
 */
@RestController
public class ContentController {
    @Autowired
    private ContentService contentService;


    @RequestMapping("/content/queryByCid/{cid}")
    public List<TbContent> queryByCid(@PathVariable long cid){
        return contentService.queryByCid(cid);
    }

    @RequestMapping("/content/save")
    public BuyResult save(@RequestBody TbContent content){
        return contentService.save(content);
    }

    @RequestMapping("/content/list")
    @ResponseBody
    public EasyUIDatagrideVO<TbContent> queryList(@RequestParam(defaultValue = "1") long page, @RequestParam(defaultValue = "20") long rows, long categoryId){
        return contentService.queryPage(categoryId,page,rows);
    }
}

package com.lh.buy.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lh.buy.content.service.ContentCategoryService;
import com.lh.buy.manager.mapper.TbContentCategoryMapper;
import com.lh.buy.util.pojo.EasyUITreeNode;
import com.lh.pojo.TbContentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 19:30
 */
@Service
public class ContentCategoryServiceImpl implements ContentCategoryService {
    @Autowired
    private TbContentCategoryMapper contentCategoryMapper;

    @Override
    public List<EasyUITreeNode> querybCatByPid(long pid) {
        List<EasyUITreeNode> nodes = new ArrayList<>();
        //准备查询条件
        QueryWrapper<TbContentCategory> wrapper = new QueryWrapper<TbContentCategory>();
        //条件是 paretn_id = #{pid}
        wrapper.eq("parent_id",pid);
        wrapper.eq("status",1);
        List<TbContentCategory> cats = contentCategoryMapper.selectList(wrapper);
        //将itemCatList转换为EasyUITreeNodeList
        for (TbContentCategory cat : cats) {
            EasyUITreeNode node = new EasyUITreeNode();
            node.setId(cat.getId());
            node.setText(cat.getName());
            node.setState(cat.getIsParent()?"closed":"open");
            nodes.add(node);
        }
        return nodes;
    }

    @Override
    public TbContentCategory save(TbContentCategory tbContentCategory) {
        tbContentCategory.setStatus(1);
        tbContentCategory.setSortOrder(1);
        tbContentCategory.setIsParent(false);
        tbContentCategory.setCreated(new Date());
        tbContentCategory.setUpdated(new Date());
        contentCategoryMapper.insert(tbContentCategory);
        return tbContentCategory;
    }

    @Override
    public void update(TbContentCategory tbContentCategory) {
        contentCategoryMapper.updateById(tbContentCategory);
    }

    @Override
    public void delete(long cid) {
        TbContentCategory tbContentCategory = new TbContentCategory();
        tbContentCategory.setId(cid);
        tbContentCategory.setStatus(2);
        contentCategoryMapper.updateById(tbContentCategory);
    }
}

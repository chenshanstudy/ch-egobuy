package com.lh.buy.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lh.buy.content.service.ContentService;
import com.lh.buy.manager.mapper.TbContentMapper;
import com.lh.buy.util.json.JsonUtil;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.pojo.TbContent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 20:59
 */
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private TbContentMapper tbContentMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    //开启事务
    @Transactional(isolation = Isolation.DEFAULT,propagation = Propagation.REQUIRED)
    public BuyResult save(TbContent tbContent) {
        tbContent.setCreated(new Date());
        tbContent.setUpdated(new Date());
        if(tbContentMapper.insert(tbContent)==1){
            //清理缓存
            String key = "CONTENT_"+tbContent.getCategoryId();
            stringRedisTemplate.delete(key);
            return BuyResult.ok();
        }
        return BuyResult.error();
    }

    @Override
    public EasyUIDatagrideVO<TbContent> queryPage(long cid, long page, long rows) {
        Page<TbContent> contentPage = new Page<>();
        //设置每页大小
        contentPage.setSize(rows);
        //设置当前页码  内部会自动计算偏移量
        contentPage.setCurrent(page);
        //准备查询条件
        QueryWrapper<TbContent> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id",cid);
        contentPage =  tbContentMapper.selectPage(contentPage,wrapper);
        //准备一个EasyUIDatagrideVO
        EasyUIDatagrideVO<TbContent> datagrideVO = new EasyUIDatagrideVO<>();
        datagrideVO.setTotal(contentPage.getTotal());
        datagrideVO.setRows(contentPage.getRecords());
        return datagrideVO;
    }

    /**
     * 轮播图动态更换
     * @param cid
     * @return
     */
    @Override
    public List<TbContent> queryByCid(long cid) {
        //先准备一个key
        String key = "CONTENT_"+cid;
        //先查询缓存
        if(stringRedisTemplate.hasKey(key)){  //如果key存在就获取缓存
            String json = stringRedisTemplate.opsForValue().get(key);
            //判断是否是空字符串
            if(StringUtils.isNoneBlank(json)){
                //将json转为列表返回
                return JsonUtil.json2list(json,TbContent.class);
            }
        }
        //准备查询条件
        QueryWrapper<TbContent> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id",cid);
        List<TbContent> tbContents = tbContentMapper.selectList(wrapper);
        //将结果放入缓存中
        stringRedisTemplate.opsForValue().set(key,JsonUtil.object2json(tbContents));
        return tbContents;
    }
}

package com.lh.buy.protal.Controller;

import com.lh.buy.manager.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lh
 * @create 2021-04-20 19:15
 */
@Controller
public class IndexController {
    @Autowired
    private ContentService contentService;

    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("ad1List",contentService.queryByCid(99));
        return "index";
    }
}

package com.lh.buy.protal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lh
 * @create 2021-04-20 19:09
 */
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.lh.buy.manager.mapper")
@ComponentScan({"com.lh.buy.manager","com.lh.buy.protal"})
public class PortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(PortalApplication.class,args);
    }
}

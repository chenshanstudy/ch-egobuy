package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbContent;
import com.lh.pojo.TbItemDesc;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbContentMapper extends BaseMapper<TbContent> {
}

package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbOrderItem;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbOrderItemMapper extends BaseMapper<TbOrderItem> {

}

package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbItemCat;

/**
 * <p>
 * 商品类目 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbItemCatMapper extends BaseMapper<TbItemCat> {

}

package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbItemDesc;


/**
 * <p>
 * 商品描述表 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbItemDescMapper extends BaseMapper<TbItemDesc> {

}

package com.lh.buy.manager.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lh
 * @create 2021-04-17 19:35
 * 分页插件
 */
@Configuration
public class PaginationConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        //拦截器集合
        MybatisPlusInterceptor interceptors = new MybatisPlusInterceptor();
        //加入分页插件的拦截器
        interceptors.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptors;
    }
}

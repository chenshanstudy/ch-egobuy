package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbOrderShipping;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbOrderShippingMapper extends BaseMapper<TbOrderShipping> {

}

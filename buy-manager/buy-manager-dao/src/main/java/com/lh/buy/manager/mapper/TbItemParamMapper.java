package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbItemParam;


/**
 * <p>
 * 商品规则参数 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbItemParamMapper extends BaseMapper<TbItemParam> {

}

package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbItemParamItem;


/**
 * <p>
 * 商品规格和商品的关系表 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbItemParamItemMapper extends BaseMapper<TbItemParamItem> {

}

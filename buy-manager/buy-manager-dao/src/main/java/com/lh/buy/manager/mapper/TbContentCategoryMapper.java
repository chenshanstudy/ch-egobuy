package com.lh.buy.manager.mapper;

import com.lh.pojo.TbContentCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 内容分类 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbContentCategoryMapper extends BaseMapper<TbContentCategory> {

}

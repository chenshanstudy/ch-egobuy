package com.lh.buy.manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.pojo.TbUser;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author lh
 * @since 2021-04-16
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}

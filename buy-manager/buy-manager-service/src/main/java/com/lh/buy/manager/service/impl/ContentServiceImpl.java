package com.lh.buy.manager.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lh.buy.manager.mapper.TbContentMapper;

import com.lh.buy.manager.mapper.TbItemDescMapper;
import com.lh.buy.manager.service.ContentService;
import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbContent;

import com.lh.pojo.TbItemDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author lh
 * @create 2021-04-20 20:59
 */
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private TbContentMapper tbContentMapper;

    @Override
    public BuyResult save(TbContent tbContent) {
        tbContent.setCreated(new Date());
        tbContent.setUpdated(new Date());
        int insert = tbContentMapper.insert(tbContent);
        if(insert==1){
            return BuyResult.ok();
        }
        return BuyResult.error();
    }

    @Override
    public EasyUIDatagrideVO<TbContent> queryPage(long cid, long page, long rows) {
        Page<TbContent> contentPage = new Page<>();
        //设置每页大小
        contentPage.setSize(rows);
        //设置当前页码  内部会自动计算偏移量
        contentPage.setCurrent(page);
        //准备查询条件
        QueryWrapper<TbContent> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id",cid);
        contentPage =  tbContentMapper.selectPage(contentPage,wrapper);
        //准备一个EasyUIDatagrideVO
        EasyUIDatagrideVO<TbContent> datagrideVO = new EasyUIDatagrideVO<>();
        datagrideVO.setTotal(contentPage.getTotal());
        datagrideVO.setRows(contentPage.getRecords());
        return datagrideVO;
    }

    /**
     * 轮播图动态更换
     * @param cid
     * @return
     */
    @Override
    public List<TbContent> queryByCid(long cid) {
        //准备查询条件
        QueryWrapper<TbContent> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id",cid);
        return tbContentMapper.selectList(wrapper);
    }
}

package com.lh.buy.manager.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lh.buy.manager.mapper.TbItemCatMapper;
import com.lh.buy.manager.service.ItemCatService;
import com.lh.buy.util.pojo.EasyUITreeNode;
import com.lh.pojo.TbItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lh
 * @create 2021-04-18 13:54
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {
    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Override
    public List<EasyUITreeNode> queryCatByParentId(long pid) {
        ArrayList<EasyUITreeNode> list = new ArrayList<>();
        QueryWrapper<TbItemCat> queryWrapper = new QueryWrapper<>();
        // 条件是 paretn_id = #{pid}
        queryWrapper.eq("parent_id",pid);
        List<TbItemCat> tbItemCats = tbItemCatMapper.selectList(queryWrapper);
        for (TbItemCat itemCat : tbItemCats) {
            EasyUITreeNode node = new EasyUITreeNode();
            node.setId(itemCat.getId());
            node.setText(itemCat.getName());
            node.setState(itemCat.getIsParent()?"closed":"open");
            list.add(node);
        }
        return list;
    }
}

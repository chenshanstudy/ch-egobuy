package com.lh.buy.manager.service;

import com.lh.buy.util.pojo.EasyUITreeNode;
import com.lh.pojo.TbContentCategory;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 19:29
 */
public interface ContentCategoryService {
    List<EasyUITreeNode> querybCatByPid(long pid);
    /**
     * 添加
     */
    TbContentCategory save(TbContentCategory tbContentCategory);
    /**
     * 修改
     */
    void update(TbContentCategory tbContentCategory);
    /**
     * 删除
     */
    void delete(long cid);
}

package com.lh.buy.manager.service;

import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbItem;

/**
 * @author lh
 * @create 2021-04-16 18:23
 */
public interface ItemService {
    TbItem queryById(long itemId);

    EasyUIDatagrideVO<TbItem> queryByPage(long  page, long rows);

    /**
     * 添加商品信息
     */
    BuyResult save(TbItem tbItem,String desc);
}

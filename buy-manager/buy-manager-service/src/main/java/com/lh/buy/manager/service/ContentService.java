package com.lh.buy.manager.service;

import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbContent;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 20:58
 */
public interface ContentService {
    /**
     * 添加商品信息
     */
    BuyResult save(TbContent tbContent);
    /**
     * 分页查询
     */
    EasyUIDatagrideVO<TbContent> queryPage(long cid, long  page, long rows);

    /**
     * 轮播图更换
     */
    List<TbContent> queryByCid(long cid);

}

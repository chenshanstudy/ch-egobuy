package com.lh.buy.manager.service;

import com.lh.buy.util.pojo.EasyUITreeNode;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-18 13:52
 */
public interface ItemCatService {
     //根据父id查询类目列表
    List<EasyUITreeNode> queryCatByParentId(long pid);

}

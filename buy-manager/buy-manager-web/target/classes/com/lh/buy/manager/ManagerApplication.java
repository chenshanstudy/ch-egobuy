package com.lh.buy.manager;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lh
 * @create 2021-04-16 18:29
 */
@SpringBootApplication(scanBasePackages = {"com.lh.buy.manager","com.lh.buy.util.fdfs"})
@MapperScan("com.lh.buy.manager.mapper")
@EnableTransactionManagement
public class ManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManagerApplication.class,args);
    }
}

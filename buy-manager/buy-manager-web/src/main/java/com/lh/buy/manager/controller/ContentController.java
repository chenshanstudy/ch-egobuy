package com.lh.buy.manager.controller;

import com.lh.buy.manager.service.ContentService;
import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * @author lh
 * @create 2021-04-20 21:10
 */
@Controller
public class ContentController {
    @Autowired
    private ContentService contentService;

    @Autowired
    private RestTemplate restTemplate;
    @Value("${contentService}")
    private String contentServiceName;

    /**
     * 添加
     * @param tbContent
     * @return
     */
    @RequestMapping("/content/save")
    @ResponseBody
    public BuyResult save(TbContent tbContent){
        return restTemplate.postForObject("http://"+contentServiceName+"/content/save",tbContent,BuyResult.class);
    }
    /**
     * 分页
     */
    @RequestMapping("/content/query/list")
    @ResponseBody
    public EasyUIDatagrideVO<TbContent> queryPage(@RequestParam(defaultValue = "1") long page, @RequestParam(defaultValue = "30") long rows, long categoryId){
        return restTemplate.getForObject("http://"+contentServiceName+"/content/list?page="+page+"&rows="+rows+"&categoryId="+categoryId, EasyUIDatagrideVO.class);
    }
}

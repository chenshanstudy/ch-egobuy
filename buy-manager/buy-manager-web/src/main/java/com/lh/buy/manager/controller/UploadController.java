package com.lh.buy.manager.controller;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.lh.buy.util.fdfs.FastdfsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lh
 * @create 2021-04-19 19:13
 * 图片上传测试
 */
@Controller
public class UploadController {

    @Autowired
    @Qualifier("fastdfsUtils")
    private FastdfsUtils fastdfsUtils;

    @Value("${imageServer}")
    private String imageServer;

    @RequestMapping("/upload")
    @ResponseBody
    public String upload(@RequestParam("picFile") MultipartFile file) throws IOException {
        //将文件上传到图片服务器
        StorePath storePath = fastdfsUtils.upload(file);
        //返回图片的访问地址
        return "http://"+imageServer+"/"+storePath.getFullPath();
    }

    @RequestMapping(value = "/pic/upload",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Map uploadk(MultipartFile uploadFile) throws IOException {
        //将文件上传到图片服务器
        StorePath storePath =  fastdfsUtils.upload(uploadFile);
        Map result = new HashMap();
        result.put("error",0);
        result.put("url","http://"+imageServer+"/"+storePath.getFullPath());
        return result;
    }


}

package com.lh.buy.manager.controller;

import com.lh.buy.manager.service.ItemCatService;
import com.lh.buy.util.pojo.EasyUITreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-18 14:08
 */
@Controller
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;

    @RequestMapping("item/cat/list")
    @ResponseBody
    public List<EasyUITreeNode> queryItemCats(@RequestParam(defaultValue = "0",value = "id")long pid){
        return itemCatService.queryCatByParentId(pid);
    }

}

package com.lh.buy.manager.controller;

import com.lh.buy.manager.service.ContentCategoryService;
import com.lh.buy.util.pojo.EasyUITreeNode;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbContentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-20 19:36
 */
@Controller
public class ContentCategoryController {
    @Autowired
    private ContentCategoryService contentCategoryService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${contentService}")
    private String contentService;

    @RequestMapping("/content/category/list")
    @ResponseBody
    public List<EasyUITreeNode> queryCats(@RequestParam(defaultValue = "0",value = "id") long pid){
        return restTemplate.getForObject("http://"+contentService+"/contentCategory/list?pid="+pid,List.class);
    }

    @RequestMapping("/content/category/create")
    public BuyResult save(TbContentCategory tbContentCategory){
        return restTemplate.postForObject("http://"+contentService+"/contentCategory/save",tbContentCategory,BuyResult.class);
    }

    @RequestMapping("/content/category/update")
    public String update(TbContentCategory tbContentCategory){
        return restTemplate.postForObject("http://"+contentService+"/contentCategory/update", tbContentCategory, String.class);
    }

    @RequestMapping("/content/category/delete")
    public String deleteCat(@RequestParam("id") long cid){
        restTemplate.delete("http://"+contentService+"/contentCategory/delete?catId="+cid);
        return "ok";
    }

}

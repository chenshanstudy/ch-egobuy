package com.lh.buy.manager.controller;

import com.lh.buy.manager.service.ItemService;
import com.lh.buy.util.pojo.EasyUIDatagrideVO;
import com.lh.buy.util.pojo.BuyResult;
import com.lh.pojo.TbItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author lh
 * @create 2021-04-16 18:26
 */
@Controller
public class ItemController {
    @Resource
    private ItemService itemService;

    @RequestMapping("item/{id}")
    @ResponseBody
    public TbItem item(@PathVariable("id") long itemId){
        return itemService.queryById(itemId);
    }

    //分页查询
    @RequestMapping("/item/list")
    @ResponseBody
    public EasyUIDatagrideVO<TbItem> queryByPage(@RequestParam(defaultValue = "1") long page, @RequestParam(defaultValue = "30") long rows){
        return itemService.queryByPage(page,rows);
    }
    //新增商品
    @RequestMapping("/item/save")
    @ResponseBody
    public BuyResult save(TbItem tbItem,String desc){
        return itemService.save(tbItem,desc);
    }
}

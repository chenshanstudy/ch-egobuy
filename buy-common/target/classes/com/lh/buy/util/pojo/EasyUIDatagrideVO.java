package com.lh.buy.util.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lh
 * @create 2021-04-17 19:32
 *  存储easyUI Datagride的数据格式
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EasyUIDatagrideVO<T> {
    private long total;
    private List<T> rows;
}

package com.lh.buy.util.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lh
 * @create 2021-04-20 15:57
 * 封装返回值结果
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuyResult {
    private String msg; //响应消息
    private int status; //响应状态码
    private Object data;//响应消息

    /**
     * 快速返回一个状态为200的BuyResult
     */
    public static BuyResult ok(){
        BuyResult result = new BuyResult();
        result.setStatus(200);
        result.setMsg("ok");
        return result;
    }
    public static BuyResult error(){
        BuyResult result = new BuyResult();
        result.setStatus(500);
        result.setMsg("error");
        return result;
    }
}

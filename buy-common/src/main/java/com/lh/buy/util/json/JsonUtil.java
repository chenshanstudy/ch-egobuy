package com.lh.buy.util.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * @author lh
 * @create 2021-04-22 20:30
 */
public class JsonUtil {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String object2json(Object obj){
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T json2object(String resource,Class<T> clzz){
        try {
            return objectMapper.readValue(resource,clzz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> json2list(String resources, Class<T> clzz){
        try {
            return objectMapper.readValue(resources, new TypeReference<List<T>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

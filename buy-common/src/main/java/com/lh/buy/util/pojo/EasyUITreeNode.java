package com.lh.buy.util.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lh
 * @create 2021-04-18 13:49
 * 描述tree的节点信息
 */
@Data
public class EasyUITreeNode implements Serializable {
    private long id;
    private String text;  //商品名字
    private String state;
}
